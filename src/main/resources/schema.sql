
CREATE TABLE IF NOT EXISTS COMPETITION
(
    title VARCHAR(50) PRIMARY KEY,
    year_competition  INT
);

CREATE TABLE IF NOT EXISTS TEAM
(
    name    VARCHAR(50) PRIMARY KEY,
    country VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS PARTICIPATES
(
    competition_title   VARCHAR(50) references COMPETITION(title),
    team_name   VARCHAR(50) references TEAM(name),
    favoris INT,
    primary key (competition_title, team_name)
);
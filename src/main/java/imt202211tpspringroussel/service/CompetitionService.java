package imt202211tpspringroussel.service;

import imt202211tpspringroussel.model.*;
import imt202211tpspringroussel.repository.CompetitionRepository;
import imt202211tpspringroussel.repository.ParticipatesRepository;
import imt202211tpspringroussel.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CompetitionService {
    private CompetitionRepository competitionRepository;
    private TeamRepository teamRepository;

    private ParticipatesRepository participatesRepository;

    @Autowired
    public CompetitionService(CompetitionRepository competitionRepository, TeamRepository teamRepository, ParticipatesRepository participatesRepository) {
        this.competitionRepository = competitionRepository;
        this.teamRepository = teamRepository;
        this.participatesRepository = participatesRepository;
    }

    public List<Competition> getAllCompetition() {
        return competitionRepository.findAll();
    }

    public Optional<Competition> getCompetitionByID(String title) {
        return competitionRepository.findById(title);
    }

    public Competition createCompetition(Competition competition) {
        if (!competitionRepository.existsById(competition.getTitle())) {
            return competitionRepository.save(competition);
        }
        return competition;
    }

    public List<Competition> createManyCompetition(List<Competition> competitions) {
        return competitionRepository.saveAll(competitions);
    }

    public Optional<Competition> deleteCompetition(String title) {
        Optional<Competition> competition = competitionRepository.findById(title);
        if (competition.isPresent()) {
            competitionRepository.delete(competition.get());
            return competition;
        }
        return Optional.empty();
    }

    public Optional<Competition> updateCompetition(String title, Competition competitionBody) {
        Optional<Competition> competitionFound = competitionRepository.findById(title);
        if (competitionFound.isEmpty()) {
            return competitionFound;
        }
        Competition competition = competitionFound.get();
        competition.setYearCompetition(competitionBody.getYearCompetition());
        competition.setTitle(competitionBody.getTitle());
        return Optional.of(competitionRepository.save(competition));
    }

    public CompetitionWithTeams addCompetitionWithTeams(CompetitionWithTeams competitionWithTeams) {
        Competition competition = competitionRepository.save(competitionWithTeams.getCompetition());
        System.out.println(competitionWithTeams.getTeamsWithFavoris());
        for (TeamWithFavoris teamWithFavoris : competitionWithTeams.getTeamsWithFavoris()) {
            teamRepository.save(teamWithFavoris.getTeam());
            participatesRepository.saveAndFlush(new Participates(competition.getTitle(), teamWithFavoris.getTeam().getName(), teamWithFavoris.getFavoris()));
        }
        return competitionWithTeams;
    }

    public List<TeamWithCompetition> getTeamWithCompetitions(String country) {
        List<TeamWithCompetition> listTeams = new ArrayList<>();
        teamRepository.findAllByCountry(country).forEach(team -> {
            TeamWithCompetition teamWithCompetition = new TeamWithCompetition();
            teamWithCompetition.setTeam(team);
            List<Competition> competitions = competitionRepository.getCompetitionByTeam(team.getName());
            teamWithCompetition.getCompetitions().addAll(competitions);
            listTeams.add(teamWithCompetition);
        });

        return listTeams;
    }
}

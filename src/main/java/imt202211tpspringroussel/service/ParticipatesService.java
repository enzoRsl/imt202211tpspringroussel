package imt202211tpspringroussel.service;

import imt202211tpspringroussel.model.Participates;
import imt202211tpspringroussel.model.ParticipatesId;
import imt202211tpspringroussel.repository.ParticipatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipatesService {

    private ParticipatesRepository participatesRepository;

    @Autowired
    public ParticipatesService(ParticipatesRepository participateRepository) {
        this.participatesRepository = participateRepository;
    }


    public Participates createParticipate(Participates participates) {
        return participatesRepository.save(participates);
    }

    public List<Participates> getAllParticipates() {
        return participatesRepository.findAll();
    }


    public Optional<Participates> getParticipatesById(String name, String title) {
        ParticipatesId participatesId = new ParticipatesId(name, title);
        return participatesRepository.findById(participatesId);
    }
}

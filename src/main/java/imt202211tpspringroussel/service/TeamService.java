package imt202211tpspringroussel.service;

import imt202211tpspringroussel.model.Competition;
import imt202211tpspringroussel.model.Participates;
import imt202211tpspringroussel.model.Team;
import imt202211tpspringroussel.repository.CompetitionRepository;
import imt202211tpspringroussel.repository.ParticipatesRepository;
import imt202211tpspringroussel.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TeamService {
    private TeamRepository teamRepository;
    private CompetitionRepository competitionRepository;
    private ParticipatesRepository participatesRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository, CompetitionRepository competitionRepository,
                       ParticipatesRepository participatesRepository) {
        this.teamRepository = teamRepository;
        this.participatesRepository = participatesRepository;
        this.competitionRepository = competitionRepository;
    }


    public List<Team> getAllTeam() {
        return teamRepository.findAll();
    }

    public Team createTeam(Team team) {
        if (!teamRepository.existsById(team.getName())) {
            return teamRepository.save(team);
        }
        return team;
    }

    public List<Team> getTeamsByCompetitionByFavoris(String title, int favoris) {
        return teamRepository.getTeamsByCompetitionByFavoris(title,favoris);
    }
}

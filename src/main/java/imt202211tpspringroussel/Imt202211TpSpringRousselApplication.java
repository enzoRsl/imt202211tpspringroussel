package imt202211tpspringroussel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@AutoConfigureDataJpa
@EnableJpaRepositories(basePackages = "imt202211tpspringroussel.repository")
public class Imt202211TpSpringRousselApplication {

    public static void main(String[] args) {
        SpringApplication.run(Imt202211TpSpringRousselApplication.class, args);
    }

}

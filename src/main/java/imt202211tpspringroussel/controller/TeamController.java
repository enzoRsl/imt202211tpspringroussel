package imt202211tpspringroussel.controller;

import imt202211tpspringroussel.model.Team;
import imt202211tpspringroussel.repository.CompetitionRepository;
import imt202211tpspringroussel.service.CompetitionService;
import imt202211tpspringroussel.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/team")
public class TeamController {
    private TeamService teamService;
    private CompetitionService competitionService;

    @Autowired
    public TeamController(TeamService teamService, CompetitionService competitionService) {
        this.teamService = teamService;
        this.competitionService = competitionService;
    }

    @GetMapping("/competition/{title}/favorisup/{favoris}")
    public ResponseEntity<List<Team>> getTeamsByCompetitionByFavoris(@PathVariable String title, @PathVariable int favoris) {
        if(this.competitionService.getCompetitionByID(title).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<Team> teams = teamService.getTeamsByCompetitionByFavoris(title, favoris);
           return ResponseEntity.ok(teams);
    }
}
package imt202211tpspringroussel.controller;

import imt202211tpspringroussel.model.*;
import imt202211tpspringroussel.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/competition")
public class CompetitionController {

    private CompetitionService competitionService;

    @Autowired
    public CompetitionController(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }


    @GetMapping("")
    public List<Competition> getAllCompetition() {
        return competitionService.getAllCompetition();
    }

    @GetMapping("/{title}")
    public ResponseEntity<Competition> getCompetition(@PathVariable String title) {
        return competitionService.getCompetitionByID(title).map(
                competition -> ResponseEntity.ok().body(competition)
        ).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("")
    public ResponseEntity<Competition> createCompetition(@RequestBody @Valid Competition competition) {
        Competition competition1 = competitionService.createCompetition(competition);
        System.out.println(competition1);
        return ResponseEntity.ok().body(competition1);
    }

    @PutMapping("/{title}")
    public ResponseEntity<Competition> updateCompetition(@PathVariable String title, @RequestBody @Valid Competition competitionBody) {
        Optional<Competition> competitionFound = competitionService.getCompetitionByID(title);
        if (!competitionFound.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return competitionService.updateCompetition(title, competitionBody).map(
                competition -> ResponseEntity.ok().body(competition)
        ).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{title}")
    public ResponseEntity<Competition> deleteCompetition(@PathVariable String title) {
        return competitionService.deleteCompetition(title).map(
                competition -> ResponseEntity.ok().body(competition)
        ).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/competitionWithTeams")
    public CompetitionWithTeams addCompetitionWithTeams(@Valid @RequestBody CompetitionWithTeams competitionWithTeams) {
        return competitionService.addCompetitionWithTeams(competitionWithTeams);
    }

        @GetMapping("/teamWithCompetitions")
    public List<TeamWithCompetition> getTeamWithCompetitions(@RequestParam String country) {
        return competitionService.getTeamWithCompetitions(country);
    }



}

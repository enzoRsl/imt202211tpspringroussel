package imt202211tpspringroussel.controller;

import imt202211tpspringroussel.model.Participates;
import imt202211tpspringroussel.service.ParticipatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/participates")
public class ParticipatesController {

    private ParticipatesService participatesService;


    @Autowired
    public ParticipatesController(ParticipatesService participatesService) {
        this.participatesService = participatesService;
    }

    @GetMapping("")
    public List<Participates> getAllParticipates() {
        return participatesService.getAllParticipates();
    }

    @GetMapping("/team/{name}/competition/{title}")
    public Optional<Participates> getParticipatesByTeamAndTitle(@PathVariable String name, @PathVariable String title) {
        return participatesService.getParticipatesById(name, title);
    }



    @PostMapping("")
    public Participates createParticipate(@RequestBody Participates participates) {
        System.out.println("ParticipatesController.createParticipate");
        return participatesService.createParticipate(participates);
    }

}

package imt202211tpspringroussel;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "imt202211tpspringroussel")
public class SpringConfiguration {
}

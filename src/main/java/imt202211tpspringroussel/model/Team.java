package imt202211tpspringroussel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class Team {
    @Id
    @NotBlank(message = "Team name cannot be blank")
    private String name;

    @NotBlank(message = "Team country cannot be blank")
    private String country;

    public Team(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public Team() {

    }

    public String getName() {
        return this.name;
    }

    public String getCountry() {
        return this.country;
    }
}

package imt202211tpspringroussel.model;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CompetitionWithTeams {

    @NotNull(message = "Competition cannot be null")
    private Competition competition;

    @NotNull(message = "TeamsWithFavoris cannot be null")
    private List<TeamWithFavoris> teamsWithFavoris;

    public CompetitionWithTeams(Competition competition, List<TeamWithFavoris> teamsWithFavoris) {
        this.competition = competition;
        this.teamsWithFavoris = teamsWithFavoris;
    }


    public Competition getCompetition() {
        return competition;
    }

    public List<TeamWithFavoris> getTeamsWithFavoris() {
        return teamsWithFavoris;
    }
}

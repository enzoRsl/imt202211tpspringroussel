package imt202211tpspringroussel.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TeamWithFavoris {

    @NotNull(message = "Team cannot be null")
    private Team team;

    @NotNull(message = "Favoris cannot be blank")
    private int favoris;

    public TeamWithFavoris(Team team, int favoris) {
        this.team = team;
        this.favoris = favoris;
    }

    public Team getTeam() {
        return team;
    }

    public int getFavoris() {
        return favoris;
    }
}

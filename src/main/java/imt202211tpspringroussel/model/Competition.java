package imt202211tpspringroussel.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Entity
public class Competition {

    @Id
    @NotBlank( message = "Competition title cannot be blank")
    private String title;

    @NotNull( message = "Competition year cannot be null")
    @Column(name = "year_competition")
    private int yearCompetition;


    public Competition(int year_competition, String title) {
        this.yearCompetition = year_competition;
        this.title = title;
    }

    public Competition() {
    }

    public int getYearCompetition() {
        return yearCompetition;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYearCompetition(int yearCompetition) {
        this.yearCompetition = yearCompetition;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Competition)) {
            return false;
        }
        Competition competition = (Competition) o;
        return competition.title.equals(title) && competition.yearCompetition == yearCompetition;
    }

}

package imt202211tpspringroussel.model;

import java.io.Serializable;
import java.util.Objects;

public class ParticipatesId implements Serializable {

    private String competitionTitle;

    private String teamName;

    public ParticipatesId(String competition, String team){
        this.competitionTitle = competition;
        this.teamName = team;
    }

    public ParticipatesId(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipatesId participatesId = (ParticipatesId) o;
        return competitionTitle.equals(participatesId.competitionTitle) &&
                teamName.equals(participatesId.teamName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(competitionTitle, teamName);
    }

}
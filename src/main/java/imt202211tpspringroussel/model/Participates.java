package imt202211tpspringroussel.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@IdClass(ParticipatesId.class)
public class Participates {

    @Id
    @Column(name = "team_name")
    @NotBlank(message = "Team name cannot be blank")
    String teamName;

    @Id
    @Column(name = "competition_title")
    @NotBlank(message = "Competition title cannot be blank")
    String competitionTitle;

    @NotNull(message = "Favorite cannot be blank")
    private int favoris;

    public Participates(String competition, String team, int favoris) {
        this.competitionTitle = competition;
        this.teamName = team;
        this.favoris = favoris;
    }

    public Participates() {

    }

    public int getFavoris() {
        return favoris;
    }

    public String getCompetitionTitle() {
        return competitionTitle;
    }
}

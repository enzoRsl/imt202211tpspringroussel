package imt202211tpspringroussel.model;

import java.util.ArrayList;
import java.util.List;

public class TeamWithCompetition {

    private Team team;
    private List<Competition> competitions;

    public TeamWithCompetition() {
        this.competitions = new ArrayList<>();
    }

    public Team getTeam() {
        return team;
    }

    public List<Competition> getCompetitions() {
        return competitions;
    }


    public TeamWithCompetition(Team team, List<Competition> competitions) {
        this.team = team;
        this.competitions = competitions;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}

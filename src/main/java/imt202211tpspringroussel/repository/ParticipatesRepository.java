package imt202211tpspringroussel.repository;

import imt202211tpspringroussel.model.Participates;
import imt202211tpspringroussel.model.ParticipatesId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ParticipatesRepository extends JpaRepository<Participates, ParticipatesId> {
    List<Participates> findAllByTeamName(String name);
}

package imt202211tpspringroussel.repository;

import imt202211tpspringroussel.model.Competition;
import imt202211tpspringroussel.model.CompetitionWithTeams;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetitionRepository extends JpaRepository<Competition, String> {
    @Query(value = "select comp from Participates p join Competition comp on comp.title = p.competitionTitle where p.teamName = :teamName")
    List<Competition> getCompetitionByTeam(@Param("teamName") String teamName);
}

package imt202211tpspringroussel.repository;

import imt202211tpspringroussel.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team,String> {
    List<Team> findAllByCountry(String country);

    @Query("SELECT t FROM Participates p join Team t on t.name = p.teamName WHERE p.competitionTitle = :competitionName AND p.favoris >= :favoris")
    List<Team> getTeamsByCompetitionByFavoris(@Param("competitionName") String competitionName, @Param("favoris") int favoris);
}

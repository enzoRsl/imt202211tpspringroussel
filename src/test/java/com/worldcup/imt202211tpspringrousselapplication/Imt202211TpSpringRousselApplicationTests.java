package com.worldcup.imt202211tpspringrousselapplication;

import imt202211tpspringroussel.Imt202211TpSpringRousselApplication;
import imt202211tpspringroussel.SpringConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { SpringConfiguration.class})
class Imt202211TpSpringRousselApplicationTests {

    @Test
    void contextLoads() {
    }

}

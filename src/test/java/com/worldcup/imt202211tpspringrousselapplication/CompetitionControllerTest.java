package com.worldcup.imt202211tpspringrousselapplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import imt202211tpspringroussel.SpringConfiguration;
import imt202211tpspringroussel.model.Competition;
import imt202211tpspringroussel.model.Participates;
import imt202211tpspringroussel.model.Team;
import imt202211tpspringroussel.service.CompetitionService;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {SpringConfiguration.class})
@WebMvcTest
public class CompetitionControllerTest {

    @MockBean
    private CompetitionService competitionService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllCompetition() throws Exception {
        given(competitionService.getAllCompetition()).willReturn(List.of(new Competition(1, "competition57"), new Competition(2, "competition57")));
        assert this.mockMvc.perform(get("/competition"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition57");
    }

    @Test
    public void testGetCompetitionById() throws Exception {
        given(competitionService.getCompetitionByID("competition57")).willReturn(of(new Competition(1, "competition57")));
        assert this.mockMvc.perform(get("/competition/competition57"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition57");
    }

    @Test
    public void testCreateCompetition() throws Exception {
        Competition competition = new Competition(1, "competition57");
        given(competitionService.createCompetition(any())).willReturn(competition);
        System.out.println(new ObjectMapper().writeValueAsString(competition));
        assert this.mockMvc.perform(post("/competition")
                        .contentType("application/json")
                        .content(new ObjectMapper().writeValueAsString(competition)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition57");
    }


    @Test
    public void testDeleteCompetition() throws Exception {
        Competition competition = new Competition(1, "competition1");
        given(competitionService.deleteCompetition("competition1")).willReturn(Optional.of(competition));
        assert this.mockMvc.perform(delete("/competition/competition1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition1");
    }

}

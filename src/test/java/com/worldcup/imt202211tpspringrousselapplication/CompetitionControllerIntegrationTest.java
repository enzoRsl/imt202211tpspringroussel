package com.worldcup.imt202211tpspringrousselapplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import imt202211tpspringroussel.SpringConfiguration;
import imt202211tpspringroussel.model.*;
import imt202211tpspringroussel.repository.CompetitionRepository;
import imt202211tpspringroussel.repository.ParticipatesRepository;
import imt202211tpspringroussel.repository.TeamRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebMvcTest
@ContextConfiguration(classes = {SpringConfiguration.class})
public class CompetitionControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CompetitionRepository competitionRepository;
    @Autowired
    private ParticipatesRepository participatesRepository;
    @Autowired
    private TeamRepository teamRepository;

    @Test
    public void testGetAllCompetition() throws Exception {
        MvcResult mvcResult= this.mockMvc.perform(get("/competition"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        assert mvcResult.getResponse().getContentAsString().contains("competition1");
        assert mvcResult.getResponse().getContentAsString().contains("2022");
    }

    @Test
    public void testGetCompetitionById() throws Exception {
        assert this.mockMvc.perform(get("/competition/competition1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition1");
    }

    @Test
    public void testCreateCompetition() throws Exception {
        Competition competition = new Competition(1, "competition57");
        System.out.println(new ObjectMapper().writeValueAsString(competition));
        assert this.mockMvc.perform(post("/competition")
                        .contentType("application/json")
                        .content(new ObjectMapper().writeValueAsString(competition)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition57");
    }

    @Test
    public void testDeleteCompetition() throws Exception {
        Competition competition = new Competition(1, "competition1");
        assert this.mockMvc.perform(delete("/competition/competition1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().contains("competition1");
    }

    @Test
    public void testCreateCompetitionWithTeams() throws Exception {
        Competition competitionBody = new Competition(2013, "competitionTestTeams");
        TeamWithFavoris auxerre = new TeamWithFavoris(new Team("Auxerre", "France"), 18);
        TeamWithFavoris lyon = new TeamWithFavoris(new Team("Lyon", "France"), 49);
        CompetitionWithTeams competitionWithTeams = new CompetitionWithTeams(competitionBody, List.of(auxerre, lyon));
        MvcResult result = this.mockMvc.perform(post("/competition/competitionWithTeams")
                        .contentType("application/json")
                        .content(new ObjectMapper().writeValueAsString(competitionWithTeams)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        competitionRepository.findById("competitionTestTeams").ifPresent(competition -> {
            assert competition.equals(competitionBody);
        });

        assert participatesRepository.findById(new ParticipatesId("competitionTestTeams", "Auxerre")).get().getFavoris() == 18;
        assert participatesRepository.findById(new ParticipatesId("competitionTestTeams", "Lyon")).get().getFavoris() == 49;
        assert teamRepository.findById("Auxerre").get().getName().equals("Auxerre");
        assert teamRepository.findById("Lyon").get().getName().equals("Lyon");

    }

    @Test
    public void testGetTeamWithCompetitions() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/competition/teamWithCompetitions?country=France"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        assert result.getResponse().getContentAsString().contains("competition1");
        assert result.getResponse().getContentAsString().contains("team1");
    }

    @Test
    public void testGetTeamsByCompetitionByFavoris() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/team/competition/competition1/favorisup/31"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        assert result.getResponse().getContentAsString().contains("team1");
        assert !result.getResponse().getContentAsString().contains("team2");
    }
}

package com.worldcup.imt202211tpspringrousselapplication;

import imt202211tpspringroussel.SpringConfiguration;
import imt202211tpspringroussel.model.Competition;
import imt202211tpspringroussel.repository.CompetitionRepository;
import imt202211tpspringroussel.service.CompetitionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

@SpringBootTest
@ContextConfiguration(classes = {SpringConfiguration.class})
public class CompetitionServiceTest {

    @MockBean
    private CompetitionRepository competitionRepository;

    @Autowired
    private CompetitionService competitionService;

    @Test
    public void testGetAllCompetition() {
        given(competitionRepository.findAll()).willReturn(List.of(new Competition(1, "competition1"), new Competition(2, "competition2")));
        assert competitionService.getAllCompetition().size() == 2;
    }

    @Test
    public void testGetCompetitionById() {
        given(competitionRepository.findById("competition1")).willReturn(Optional.of(new Competition(1, "competition1")));
        Optional<Competition> competition = competitionService.getCompetitionByID("competition1");
        assert competition.isPresent();
        assert competition.get().getTitle().equals("competition1");
    }

    @Test
    public void testCreateCompetition() {
        Competition competition = new Competition(1, "competition4");
        given(competitionRepository.save(competition)).willReturn(competition);
        Competition competitionCreated = competitionService.createCompetition(competition);
        assert competitionCreated.getTitle().equals("competition4");
    }

    @Test
    public void createManyCompetition() {
        Competition competition1 = new Competition(1, "competition1");
        Competition competition2 = new Competition(2, "competition2");
        given(competitionRepository.saveAll(List.of(competition1, competition2))).willReturn(List.of(competition1, competition2));
        List<Competition> competitions = competitionService.createManyCompetition(List.of(competition1, competition2));
        assert competitions.size() == 2;
    }

    @Test
    public void testDeleteCompetition() {
        Competition competition = new Competition(1, "competition1");
        given(competitionRepository.findById("competition1")).willReturn(Optional.of(competition));
        Optional<Competition> competitionDeleted = competitionService.deleteCompetition("competition1");
        assert competitionDeleted.isPresent();
        assert competitionDeleted.get().getTitle().equals("competition1");
    }

}